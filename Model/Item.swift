//
//  Item.swift
//  CodeChallenge
//
//  Created by soroush amini araste on 7/2/21.
//

import Foundation

struct Item: Codable {
    var id: Int
    var title: String
    var url: String
    var thumbnailUrl: String
}
