//
//  ItemsViewController.swift
//  CodeChallenge
//
//  Created by soroush amini araste on 7/5/21.
//

import UIKit

class ItemsViewController: UIViewController {
    
    var items: [Item] = [] {
        didSet {
            self.itemsCollectionView.items = items
        }
    }
    
    //Paginate Control
    var startPage: Int = 1
    var endPage: Int = 10
    
    lazy var itemsCollectionView: ItemsCollectionView = {
        var collectionView = ItemsCollectionView()
        collectionView.delegate = self
        return collectionView
    }()
    
    weak var coordinator: MainCoordinator?
    
    //MARK: - VIEW CONTROLLER LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.title = "Home"
        self.createUI()
        self.gettingItems()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if items.isEmpty {
            self.gettingItems()
        }
    }
    
    //MARK: - CREATE UI
    fileprivate func createUI() {
        self.addingItemsCollectionView()
    }
    
    fileprivate func addingItemsCollectionView() {
        self.view.addSubview(itemsCollectionView)
        self.itemsCollectionView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            itemsCollectionView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0),
            itemsCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            itemsCollectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            itemsCollectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0),
        ])
    }
    
    //MARK: - API
    fileprivate func gettingItems() {
        if startPage > 0 {
            NetworkManager.shared.fetchItems(startPage: startPage, endPage: endPage) { result in
                switch result {
                case .success(let items):
                    self.items += items
                    if items.count < 10 {
                        self.startPage = -1
                    }
                    //print(items)
                case .failure(let error):
                    print("خطایی رخ داده", error.localizedDescription)
                }
                self.startPage += 10
                self.endPage += 10
            }
        } else {
            print("داده جدیدی برای نمایش نیست")
        }
    }

}

extension ItemsViewController: ItemsCollectionViewDelegate {
    func selectedItem(item: Item) {
        self.coordinator?.showItemDetailsViewController(item: item)
    }
    
    func loadMore() {
        if startPage > 1 {
            self.gettingItems()
        }
    }
}
