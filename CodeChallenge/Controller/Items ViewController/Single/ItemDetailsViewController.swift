//
//  ItemDetailsViewController.swift
//  CodeChallenge
//
//  Created by soroush amini araste on 7/6/21.
//

import UIKit

class ItemDetailsViewController: UIViewController {
    
    var item: Item? {
        didSet {
            self.updateUI()
        }
    }
    
    lazy var itemImageView: BaseImageView = {
       var imageView = BaseImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 10
        return imageView
    }()
    weak var coordinator: MainCoordinator?
    
    //MARK: - VIEW CONTROLLER LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        title = "Details"
        self.createUI()
        // Do any additional setup after loading the view.
    }
    
    //MARK: - CREATE UI
    fileprivate func createUI() {
        self.addingItemImageView()
    }
    
    fileprivate func addingItemImageView() {
        self.view.addSubview(itemImageView)
        self.itemImageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            itemImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            itemImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            itemImageView.widthAnchor.constraint(equalTo: view.widthAnchor,multiplier: 1, constant: -20),
            itemImageView.heightAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1, constant: -20)
        ])
    }
    
    //MARK: - UPDATE UI
    fileprivate func updateUI() {
        guard let item = self.item else { return }
        self.itemImageView.loadImageUsingURLString(urlString: item.url)
    }
}
