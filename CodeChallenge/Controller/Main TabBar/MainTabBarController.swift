//
//  MainTabBarController.swift
//  CodeChallenge
//
//  Created by soroush amini araste on 7/1/21.
//

import UIKit

class MainTabBarController: UITabBarController {
    
    var tabBarListViewController: [UIViewController] = []
    
    let main = MainCoordinator(navigationController: UINavigationController())
    
    //MARK: - CONTROLLER LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        main.start()
        viewControllers = [main.navigationController]
    }
}
