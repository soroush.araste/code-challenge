//
//  ItemsCollectionDelegate.swift
//  CodeChallenge
//
//  Created by soroush amini araste on 7/8/21.
//

import Foundation
protocol ItemsCollectionViewDelegate: AnyObject {
    func selectedItem(item: Item)
    func loadMore()
}

extension ItemsCollectionViewDelegate {
    func loadMore() {}
}
