//
//  ItemsCollectionViewCell.swift
//  CodeChallenge
//
//  Created by soroush amini araste on 7/2/21.
//

import UIKit

class ItemsCollectionViewCell: UICollectionViewCell {
    
    var item: Item? {
        didSet {
            self.updateUI()
        }
    }
    
    lazy var thumbnailImageView: BaseImageView = {
        var imageView = BaseImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var titleLabel: UILabel = {
        var label = UILabel()
        label.textAlignment = .left
        label.textColor = .black
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 14)
        return label
    }()
    
    //MARK: - DEFAULT INITIALIZER
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .clear
        self.layer.borderWidth = 0.3
        self.layer.borderColor = UIColor.black.withAlphaComponent(0.8).cgColor
        self.createUI()
    }
        
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
        
    //MARK: - CREATE UI
    fileprivate func createUI() {
        self.addingThumbnailImageView()
        self.addingTitleLabel()
    }
    
    fileprivate func addingThumbnailImageView() {
        contentView.addSubview(thumbnailImageView)
        thumbnailImageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            thumbnailImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            thumbnailImageView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            thumbnailImageView.widthAnchor.constraint(equalTo:contentView.widthAnchor),
            thumbnailImageView.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0, constant: 150)
            
        ])
    }
    
    fileprivate func addingTitleLabel() {
        contentView.addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: thumbnailImageView.bottomAnchor,constant: 5),
            titleLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -10),
            titleLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 10),
        ])
    }
    //MARK: - UPDATE UI
    fileprivate func updateUI() {
        guard let item = self.item else { return }
        self.thumbnailImageView.loadImageUsingURLString(urlString: item.thumbnailUrl)
        self.titleLabel.text = item.title
    }
}
