//
//  ItemsCollectionView.swift
//  CodeChallenge
//
//  Created by soroush amini araste on 7/5/21.
//

import UIKit

class ItemsCollectionView: UIView {
    
    var items: [Item] = [] {
        didSet {
            self.mainCollectionView.reloadData()
        }
    }
    
    let cellIdentifier = "itemsCell"
    
    private lazy var collectionViewFlowLayout: UICollectionViewFlowLayout = {
        let collectionViewFlowLayout = UICollectionViewFlowLayout()
        collectionViewFlowLayout.scrollDirection = .vertical
        return collectionViewFlowLayout
    }()
    
    private lazy var mainCollectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewFlowLayout)
        collectionView.register(ItemsCollectionViewCell.self, forCellWithReuseIdentifier: cellIdentifier)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .clear
        collectionView.bounces = false
        collectionView.showsVerticalScrollIndicator = false
        return collectionView
    }()
    
    weak var delegate: ItemsCollectionViewDelegate?
    
    //MARK: - INITIALIZER FRAME
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.createUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - CREATE UI
    fileprivate func createUI() {
        self.addingMainCollectionView()
    }
    
    fileprivate func addingMainCollectionView() {
        self.addSubview(mainCollectionView)
        self.mainCollectionView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            mainCollectionView.topAnchor.constraint(equalTo: self.topAnchor),
            mainCollectionView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            mainCollectionView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            mainCollectionView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
        ])
    }
}

extension ItemsCollectionView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! ItemsCollectionViewCell
        cell.item = items[indexPath.row]
        return cell
    }
}

extension ItemsCollectionView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.selectedItem(item: items[indexPath.row])
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == items.count - 2 {
            self.delegate?.loadMore()
        }
    }
}

extension ItemsCollectionView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: 8, left: 5, bottom: 5, right: 5)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let leftAndRightInset: CGFloat = 10
        let interItemSpace: CGFloat = 10
        let labelPadding: CGFloat = 20
        let imageTopInset: CGFloat = 10
        let imageHeight: CGFloat = 150 + imageTopInset
        let width = self.mainCollectionView.frame.width / 2 - interItemSpace - leftAndRightInset
        let estimateLabelHeight = items[indexPath.row].title.height(withConstrainedWidth: width - labelPadding, font: .systemFont(ofSize: 14))
        return CGSize(width: width, height: estimateLabelHeight + imageHeight + 10)
    }
}

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return ceil(boundingBox.height)
    }
}
