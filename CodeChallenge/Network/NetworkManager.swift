//
//  NetworkHandler.swift
//  CodeChallenge
//
//  Created by soroush amini araste on 7/4/21.
//

import UIKit

class NetworkManager {
    static let shared = NetworkManager()
    
    private let baseURL = "https://my-json-server.typicode.com/amosavian/restdemo/"
    let session: URLSession
    
    init(urlSession: URLSession = .shared) {
        self.session = urlSession
    }
    
    func fetchItems(startPage: Int, endPage: Int, completion: @escaping(Result<[Item], Error>) -> ()) {
        let fullURL = baseURL + "\(startPage)-\(endPage)"
        guard let url = URL(string: fullURL) else { return }
        
        
        session.dataTask(with: url) { data, response, error in
            DispatchQueue.main.async {
                if let error = error {
                    print("هنگام دریافت اطلاعاتی خطایی رخ داد: ", error)
                    return
                }
                guard let data = data else {
                    return
                }
                do {
                    let items = try JSONDecoder().decode([Item].self, from: data)
                    completion(.success(items))
                } catch {
                    print("error on decoding json")
                    completion(.failure(error))
                }
            }
        }.resume()
    }
}
