//
//  Coordinator.swift
//  CodeChallenge
//
//  Created by soroush amini araste on 7/6/21.
//

import Foundation
import UIKit

protocol Coordinator {
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
    func start()
}
