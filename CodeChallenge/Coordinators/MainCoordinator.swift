//
//  MainCoordinator.swift
//  CodeChallenge
//
//  Created by soroush amini araste on 7/6/21.
//

import Foundation
import UIKit

class MainCoordinator: Coordinator {
    var childCoordinators = [Coordinator]()
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let vc = ItemsViewController()
        vc.coordinator = self
        vc.tabBarItem = UITabBarItem(title: "Home", image: UIImage(named: "home_icon")?.withRenderingMode(.alwaysTemplate), selectedImage: UIImage(named: "home_icon")?.withRenderingMode(.alwaysOriginal))
        navigationController.pushViewController(vc, animated: false)
    }
    
    func showItemDetailsViewController(item: Item) {
        let vc = ItemDetailsViewController()
        vc.coordinator = self
        vc.item = item
        navigationController.pushViewController(vc, animated: true)
    }
}
