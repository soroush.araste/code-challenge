//
//  BaseImageView.swift
//  CodeChallenge
//
//  Created by soroush amini araste on 7/6/21.
//

import Foundation
import UIKit

let imageCache = NSCache<AnyObject, AnyObject>()
class BaseImageView: UIImageView{
    var imageURLString: String?
    
    func loadImageUsingURLString(urlString: String){
        imageURLString = urlString
        let url = URL(string: urlString)
        image = nil
        if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) as! UIImage? {
            self.image = imageFromCache
            return
        }
        if let imageURL = url{
            URLSession.shared.dataTask(with: imageURL, completionHandler: { (data, response, error) in
                if error != nil{
                    print(error!)
                    return
                }
                DispatchQueue.main.async {
                    let imageToCache = UIImage(data: data!)
                    if self.imageURLString == urlString{
                        self.image = imageToCache
                    }
                    if let cache = imageToCache {
                        imageCache.setObject(cache, forKey: urlString as AnyObject)
                    }
                }
            }).resume()
        }
    }
}
